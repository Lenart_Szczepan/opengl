#include "okno.h"
#include <iostream>



Shaders::Shaders()
{
	this->szerokoscOkna = 1024;
	this->wysokoscOkna = 768;
	this->posX = 100;
	this->posY = 100;
}

Shaders::Shaders(int szerokoscOkna, int wysokoscOkna, int posX, int posY)
{
	this->szerokoscOkna = szerokoscOkna;
	this->wysokoscOkna = wysokoscOkna;
	this->posX = posX;
	this->posY = posY;
}

void Shaders::inicjalizacjaGlew()

{
	GLenum wynik = glewInit();
	if (wynik != GLEW_OK)
	{
		std::cerr << "NIe udalo sie zainicjalizowac GlEW.Blad: " << glewGetErrorString(wynik) << std::endl;
		system("pause");
		exit(1);
	}
}



void Shaders::stworzenieOkna(int argc, char ** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(szerokoscOkna, wysokoscOkna);
	glutInitWindowPosition(posX, posY);
	glutCreateWindow("Shaders");
}

void Shaders::Display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glEnableVertexAttribArray(0);
	
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glDrawArrays(GL_TRIANGLES, 0, 3);

	
	glDisableVertexAttribArray(0);
	glutSwapBuffers();
}

void Shaders::stworzenieVAO()
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
}
void Shaders::stworzenieVBO()
{
	GLfloat Wierzcholki[9] = { -1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		0.0f, 1.0f,0.0f};

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Wierzcholki), Wierzcholki, GL_STATIC_DRAW);
}





int main(int argc, char ** argv)
{
Shaders Shaders(786, 1024, 100, 100);



// po zamknieciu okna kontrola wraca do programu
//glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

Shaders.stworzenieOkna(argc, argv);
Shaders.inicjalizacjaGlew();



Shaders.stworzenieVAO();
Shaders.stworzenieVBO();
glutDisplayFunc(Shaders.Display);

glClearColor(0.2f, 0.1f, 0.0f, 0.0f);

glutMainLoop();
system("pause");
return 0;
}