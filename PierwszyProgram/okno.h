#include <GL\glew.h>
#include <GL\freeglut.h>

#pragma once

GLuint VBO;
GLuint VAO;


class Shaders
{
public:
	Shaders();

	Shaders(int szerokoscOkna, int wysokoscOkna, int posX, int posY);
	
	//~Shaders(void);

	

	void stworzenieOkna(int argc, char** argv);
	void inicjalizacjaGlew();


	
	void stworzenieVAO();
	void stworzenieVBO();
	static void Display();

private:
	int szerokoscOkna;
	int wysokoscOkna;
	int posX;
	int posY;
};

